# pink-gasmodes

EPICS IOC + Python script to control PINK's cryo chamber atmosphere.

**Docker tags**

v1.0:
- Original version before 12.11.24

v1.1:
- **Current version**
- Removed flush with He before pumping 
- V22 opens now from 800 mbar instead of 500
- Adjusted needle valve logic to be at 80 %