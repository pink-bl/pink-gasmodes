#!../../bin/linux-x86_64/gmodes

## You may have to change gmodes to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/gmodes.dbd"
gmodes_registerRecordDeviceDriver pdbbase

## Load record instances
dbLoadRecords("db/gmodes.db","BL=PINK, DEV=GMODES")

cd "${TOP}/iocBoot/${IOC}"

set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")

iocInit

create_monitor_set("auto_settings.req", 30, "BL=PINK, DEV=GMODES")

## Start any sequence programs
#seq sncxxx,"user=epics"
