import epics
import time

class PINKVALVE(object):
    def __init__(self, vnumber, LSWclose=False, LSWopen=False):
        self.timeout = 3
        self.pvRBV = "PINK:PLCGAS:V" + str(vnumber) + "_RBV"
        pvvalid = epics.caget(self.pvRBV, timeout=self.timeout)
        if pvvalid==None:
            self.pvvalid = False
        else:
            self.pvvalid = True
            self.pvclose = "PINK:PLCGAS:V" + str(vnumber) + "close"
            self.pvopen = "PINK:PLCGAS:V" + str(vnumber) + "open"
            self.LSWclose = LSWclose
            self.LSWopen = LSWopen
            if LSWclose==True:
                self.pvlsclose = "PINK:PLCGAS:V" + str(vnumber) + "_LS_close"
            else:
                self.pvlsclose = "PINK:PLCGAS:V" + str(vnumber) + "_RBV"
            if LSWopen==True:
                self.pvlsopen = "PINK:PLCGAS:V" + str(vnumber) + "_LS_open"
            else:
                self.pvlsopen = "PINK:PLCGAS:V" + str(vnumber) + "_RBV"

    def open(self):
        if self.pvvalid:
            try:
                epics.caput(self.pvopen, 1, timeout=self.timeout)
                for i in range(8):
                    if self.isopened():
                        return True
                    else:
                        time.sleep(0.5)
            except:
                pass
        return None

    def close(self):
        if self.pvvalid:
            try:
                epics.caput(self.pvclose, 1, timeout=self.timeout)
                for i in range(8):
                    if self.isclosed():
                        return True
                    else:
                        time.sleep(0.5)
            except:
                pass
        return None

    def isclosed(self):
        if self.pvvalid:
            try:
                pvresp = epics.caget(self.pvlsclose, timeout=self.timeout)
                if (self.LSWclose and pvresp==1) or (self.LSWclose==False and pvresp==0):
                    return True
                else:
                    return False
            except:
                pass
        return None

    def isopened(self):
        if self.pvvalid:
            try:
                pvresp = epics.caget(self.pvlsopen, timeout=self.timeout)
                if pvresp==1:
                    return True
                else:
                    return False
            except:
                pass
        return None

class PINKVALVEVAC(object):
    def __init__(self, vnumber, LSWclose=False, LSWopen=False):
        self.timeout = 3
        self.pvRBV = "PINK:PLCVAC:V" + str(vnumber) + "_RBV"
        pvvalid = epics.caget(self.pvRBV, timeout=self.timeout)
        if pvvalid==None:
            self.pvvalid = False
        else:
            self.pvvalid = True
            self.pvclose = "PINK:PLCVAC:V" + str(vnumber) + "close"
            self.pvopen = "PINK:PLCVAC:V" + str(vnumber) + "open"
            self.LSWclose = LSWclose
            self.LSWopen = LSWopen
            if LSWclose==True:
                self.pvlsclose = "PINK:PLCVAC:V" + str(vnumber) + "_LS_close"
            else:
                self.pvlsclose = "PINK:PLCVAC:V" + str(vnumber) + "_RBV"
            if LSWopen==True:
                self.pvlsopen = "PINK:PLCVAC:V" + str(vnumber) + "_LS_open"
            else:
                self.pvlsopen = "PINK:PLCVAC:V" + str(vnumber) + "_RBV"

    def open(self):
        if self.pvvalid:
            try:
                epics.caput(self.pvopen, 1, timeout=self.timeout)
                for i in range(8):
                    if self.isopened():
                        return True
                    else:
                        time.sleep(0.5)
            except:
                pass
        return None

    def close(self):
        if self.pvvalid:
            try:
                epics.caput(self.pvclose, 1, timeout=self.timeout)
                for i in range(8):
                    if self.isclosed():
                        return True
                    else:
                        time.sleep(0.5)
            except:
                pass
        return None

    def isclosed(self):
        if self.pvvalid:
            try:
                pvresp = epics.caget(self.pvlsclose, timeout=self.timeout)
                if (self.LSWclose and pvresp==1) or (self.LSWclose==False and pvresp==0):
                    return True
                else:
                    return False
            except:
                pass
        return None

    def isclosed(self):
        if self.pvvalid:
            try:
                pvresp = epics.caget(self.pvlsclose, timeout=self.timeout)
                if (self.LSWclose and pvresp==1) or (self.LSWclose==False and pvresp==0):
                    return True
                else:
                    return False
            except:
                pass
        return None

    def isopened(self):
        if self.pvvalid:
            try:
                pvresp = epics.caget(self.pvlsopen, timeout=self.timeout)
                if pvresp==1:
                    return True
                else:
                    return False
            except:
                pass
        return None

