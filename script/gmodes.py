#!/usr/bin/python3
from pinkvalves import PINKVALVE as PKV
from pinkvalves import PINKVALVEVAC as PKVAC

import gmodes_lib as gaslib
import epics
import time

### Main variables
print("Connecting PVs ...")

#Constants
pFV = 1
DEBUG=0

# cycle wait time
cwt = 0.1
# Valves GAS PLC
V20 = PKV(20, LSWclose=True, LSWopen=True)
V21 = PKV(21, LSWclose=True, LSWopen=False)
V22 = PKV(22, LSWclose=True, LSWopen=True)
V23 = PKV(23, LSWclose=True, LSWopen=True)
V26 = PKV(26, LSWclose=False, LSWopen=False)
V27 = PKV(27, LSWclose=False, LSWopen=False)
V28 = PKV(28, LSWclose=True, LSWopen=False)
V29 = PKV(29, LSWclose=True, LSWopen=False)

# Valves VAC PLC
V11 = PKVAC(11, LSWclose=False, LSWopen=False)

# Flow control valve
RV01 = epics.PV("PINK:PLCGAS:RV01_inp", auto_monitor=False)

# Gauges
G13 = epics.PV("PINK:MAXC:S1Measure", auto_monitor=True)
G16 = epics.PV("PINK:MAXC:S3Measure", auto_monitor=True)
# Other PVs
gasmodepv = epics.PV("PINK:GMODES:gas_mode", auto_monitor=True)
status_bit = epics.PV("PINK:GMODES:status_bit", auto_monitor=False)
Pch = epics.PV("PINK:GMODES:Pch", auto_monitor=True)
flow_mode = epics.PV("PINK:PLCGAS:ei_B03", auto_monitor=True)
mode_choice = epics.PV("PINK:GMODES:gmodes", auto_monitor=True)

gasmodes = {
    "idle": 1,
    "closeall": 2,
    "pump_chamber": 3,
    "vent_chamber": 4,
    "vent_flush": 5,
    "fill_He": 6,
    "test": 99
}
gasstates = {
    "idle": 0,
    "closeall": 100,
    "pump_chamber": 200,
    "vent_chamber": 300,
    "vent_flush": 400,
    "fill_He": 500,
    "test": 9900
}

## initial state/gasmode=idle
state = gasstates.get('idle')


print("Running gas modes service...")
### Main loop
while(1):
    try:
        # Idle
        if state==0:
            if DEBUG: print("state: " + str(state))
            gaslib.writestatus("idle")
            gaslib.writelog("{Idle}")
            status_bit.put(0)
            gasmodepv.put(0)
            flow_mode.put(0)
            state=1
        elif state==1:
            gm=gasmodepv.value
            if gm == gasmodes.get('idle'):
                state=gasstates.get('idle')
            if gm == gasmodes.get('closeall'):
                state=gasstates.get('closeall')
            elif gm == gasmodes.get('pump_chamber'):
                state=gasstates.get('pump_chamber')
            elif gm == gasmodes.get('vent_chamber'):
                state=gasstates.get('vent_chamber')
            elif gm == gasmodes.get('vent_flush'):
                state=gasstates.get('vent_flush')
            elif gm == gasmodes.get('fill_He'):
                state=gasstates.get('fill_He')
            elif gm == gasmodes.get('test'):
                state=gasstates.get('test')
            else:
                pass

        # Close all
        elif state==100:
            if DEBUG: print("state: " + str(state))
            gaslib.writestatus("Close all")
            gaslib.writelog("Begin: Close all mode")
            status_bit.put(1)
            state=101
        elif state==101:
            if DEBUG: print("state: " + str(state))
            V20.close()
            V21.close()
            V22.close()
            V23.close()
            V26.close()
            V27.close()
            V28.close()
            V29.close()
            #time.sleep(3)
            gaslib.writelog("End: Close all mode")
            state = gasstates.get('idle')

        # Pumping the chamber
        elif state==200:
            if DEBUG: print("state: " + str(state))
            gaslib.writestatus("Pumping down the chamber...")
            gaslib.writelog("Begin: Pump the chamber mode")
            status_bit.put(1)
            state=201
        elif state==201:
            V20.close()
            V21.close()
            V22.close()
            V23.close()
            V26.close()
            V27.close()
            if gasmodepv.value == gasmodes.get('idle'):
                state=gasstates.get('idle')
            else:
                gaslib.writestatus("Opening V23 and RV01 ...")
                state=202
        elif state==202:
            V23.open()
            RV01.put(80)
            if gasmodepv.value == gasmodes.get('idle'):
                state=gasstates.get('idle')
            else:
                gaslib.writestatus("Waiting for G16<1 mbar or G16<G13")
                state=203
        elif state==203:
            if gasmodepv.value == gasmodes.get('idle'):
                state=gasstates.get('idle')
            elif (G16.value < pFV) or (G16.value<G13.value):
                state=204
        elif state==204:
            V20.open()
            V21.open()
            if gasmodepv.value == gasmodes.get('idle'):
                state=gasstates.get('idle')
            else:
                gaslib.writestatus("Waiting for G13 <= 500 mbar")
                state=205
        elif state==205:
            if gasmodepv.value==gasmodes.get('idle'):
                state=gasstates.get('idle')
            elif G13.value < 500:
                state=206
        elif state==206:
            V22.open()
            if gasmodepv.value == gasmodes.get('idle'):
                state=gasstates.get('idle')
            else:
                gaslib.writestatus("Waiting for G13 <= 0.5 mbar")
                state=207
        elif state==207:
            if gasmodepv.value==gasmodes.get('idle'):
                state=gasstates.get('idle')
            elif G13.value <= 0.5:
                gaslib.writelog("End: Pump the chamber mode")
                state=gasstates.get('idle')

        # Venting the chamber
        elif state==300:
            if DEBUG: print("state: " + str(state))
            gaslib.writestatus("Venting the chamber...")
            gaslib.writelog("Begin: Venting the chamber mode")
            status_bit.put(1)
            state=301
        elif state==301:
            V20.close()
            V21.close()
            V22.close()
            V23.close()
            V27.close()
            V28.close()
            V29.close()
            V11.close()
            if gasmodepv.value == gasmodes.get('idle'):
                state=gasstates.get('idle')
            else:
                gaslib.writestatus("Opening V27, V20, V21 and RV01=60%")
                state=302
        elif state==302:
            V26.open()
            RV01.put(60)
            V20.open()
            V21.open()
            if gasmodepv.value == gasmodes.get('idle'):
                state=gasstates.get('idle')
            else:
                gaslib.writestatus("Waiting G13 >= 300 mbar")
                state=303
        elif state==303:
            if gasmodepv.value == gasmodes.get('idle'):
                state=gasstates.get('idle')
            elif G13.value >= 300:
                RV01.put(80)
                gaslib.writestatus("Waiting G13 >= 1150 mbar")
                state=304
        elif state==304:
            if gasmodepv.value == gasmodes.get('idle'):
                state=gasstates.get('idle')
            elif G13.value >= 1150:
                gaslib.writestatus("Closing Valves")
                gaslib.writelog("End: Venting the chamber mode")
                state=gasstates.get('closeall')

        # Vent and flush for sample exchange
        elif state==400:
            if DEBUG: print("state: " + str(state))
            gaslib.writestatus("Closing all valves")
            gaslib.writelog("Begin: Venting and flow mode")
            status_bit.put(1)
            state=401
        elif state==401:
            V20.close()
            V21.close()
            V22.close()
            V23.close()
            V26.close()
            V27.close()
            V28.close()
            V11.close()
            if gasmodepv.value == gasmodes.get('idle'):
                state=gasstates.get('idle')
            else:
                gaslib.writestatus("Flushing gas line")
                state=402
        elif state==402:
            V23.open()
            V21.open()
            V22.open()
            if gasmodepv.value == gasmodes.get('idle'):
                state=gasstates.get('idle')
            else:
                gaslib.writestatus("Waiting for G16 <= 0.5 mbar")
                state=403
        elif state==403:
            if gasmodepv.value == gasmodes.get('idle'):
                state=gasstates.get('idle')
            elif G16.value <= 0.5:
                gaslib.writestatus("Close V23/V22, RV01=80%")
                state=404
        elif state==404:
            V23.close()
            V22.close()
            RV01.put(60)
            if gasmodepv.value == gasmodes.get('idle'):
                state=gasstates.get('idle')
            else:
                gaslib.writestatus("Opening V26(He) and V20")
                state=405
        elif state==405:
            V26.open()
            time.sleep(2)
            V20.open()
            if gasmodepv.value == gasmodes.get('idle'):
                state=gasstates.get('idle')
            else:
                gaslib.writestatus("Waiting for G13 >= 100 mbar")
                state=406
        elif state==406:
            if gasmodepv.value == gasmodes.get('idle'):
                state=gasstates.get('idle')
            elif G13.value >= 100:
                RV01.put(80)
                gaslib.writestatus("Waiting for G13 >= 1100 mbar")
                state=407
        elif state==407:
            if gasmodepv.value == gasmodes.get('idle'):
                state=gasstates.get('idle')
            elif G13.value >= 1100:
                gaslib.writestatus("Waiting for G13 >= 1100 mbar")
                state=408
        elif state==408:
            V22.open()
            V20.open()
            flow_mode.put(1)
            if gasmodepv.value == gasmodes.get('idle'):
                state=gasstates.get('idle')
            else:
                gaslib.writestatus("Flow mode ON")
                gaslib.writelog("Flow mode ON")
                state=409
        elif state==409:
            if (gasmodepv.value == gasmodes.get('idle')) or (flow_mode.value==0) or (int(mode_choice.value)!=2):
                state=gasstates.get('idle')

        # Fill with He for experiment
        elif state==500:
            if DEBUG: print("state: " + str(state))
            gaslib.writestatus("Closing all valves")
            gaslib.writelog("Begin: Filling chamber with He")
            status_bit.put(1)
            state=501
        elif state==501:
            V20.close()
            V21.close()
            V22.close()
            V23.close()
            V26.close()
            V27.close()
            V28.close()
            if gasmodepv.value == gasmodes.get('idle'):
                state=gasstates.get('idle')
            else:
                gaslib.writestatus("Close V20, Open V21/22, RV01=60%")
                state=502
        elif state==502:
            V20.close()
            V21.open()
            V22.open()
            RV01.put(60)
            if gasmodepv.value == gasmodes.get('idle'):
                state=gasstates.get('idle')
            else:
                gaslib.writestatus("Pumping chamber")
                state=503
        elif state==503:
            V23.open()
            if gasmodepv.value == gasmodes.get('idle'):
                state=gasstates.get('idle')
            else:
                gaslib.writestatus("Waiting G16 < 0.5 mbar")
                state=504
        elif state==504:
            if gasmodepv.value==gasmodes.get('idle'):
                state=gasstates.get('idle')
            elif G16.value <= 0.5:
                gaslib.writestatus("Flushing gas line")
                Nrepeat=1
                state=505
        elif state==505:
            V23.close()
            V26.open()
            if gasmodepv.value==gasmodes.get('idle'):
                state=gasstates.get('idle')
            else:
                gaslib.writestatus("Flushing gas line " + '{:d}'.format(int(Nrepeat)) + " times")
                state=506
        elif state==506:
            if gasmodepv.value==gasmodes.get('idle'):
                state=gasstates.get('idle')
            elif G16.value >= 500:
                #gaslib.writestatus("Waiting for 5 seconds")
                #time.sleep(5)
                state=507
        elif state==507:
            V26.close()
            V23.open()
            if gasmodepv.value == gasmodes.get('idle'):
                state=gasstates.get('idle')
            else:
                gaslib.writestatus("Waiting G16 < 0.5 mbar")
                state=508
        elif state==508:
            if gasmodepv.value==gasmodes.get('idle'):
                state=gasstates.get('idle')
            elif G16.value <= 0.5:
                #gaslib.writestatus("Flushing gas line")
                Nrepeat=Nrepeat-1
                if Nrepeat<=0:
                    gaslib.writestatus("Filling with He")
                    state=509
                else:
                    state=505
        elif state==509:
            if Pch.value<200:
                rvlevel=60
            if Pch.value<20:
                rvlevel=50
            if Pch.value<2:
                rvlevel=40
            RV01.put(rvlevel)
            V23.close()
            V22.close()
            V20.open()
            V21.open()
            V26.open()
            if gasmodepv.value == gasmodes.get('idle'):
                state=gasstates.get('idle')
            else:
                gaslib.writestatus("Waiting G13 >= " + '{:.2f}'.format(Pch.value) + " mbar")
                state=510
        elif state==510:
            if gasmodepv.value==gasmodes.get('idle'):
                state=gasstates.get('idle')
            elif G13.value >= Pch.value:
                state=511
        elif state==511:
            V20.close()
            V21.close()
            V26.close()
            gaslib.writelog("End: Filling chamber with He")
            state=gasstates.get('idle')

        # test state
        elif state==9900:
            gaslib.writestatus("Test")
            gaslib.writelog("Begin: Test mode")
            status_bit.put(1)
            state=9901
        elif state==9901:
            print("G13: "+ '{:.3e}'.format(G13.value))
            print("G16: "+ '{:.3e}'.format(G16.value))
            print("Pch: "+ '{:.2f}'.format(Pch.value) + " mbar")
            state = gasstates.get('idle')
            gaslib.writelog("End: Test mode")
            #gaslib.writelog(mystr)

        # Default state
        else:
            state = gasstates.get('idle')

        # polling waiting time
        time.sleep(cwt)

    except KeyboardInterrupt:
        print("\n[main loop]: Aborting script. \nBye")
        break
    except Exception as ex1:
        print("\n[main loop]: Exception caught. Returning to the main loop")
        print(ex1)
        gaslib.writelog("*** Exception caught ***")
        gaslib.writelog(str(ex1))
        state=gasstates.get('idle')
        time.sleep(5)
