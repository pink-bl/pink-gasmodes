import epics
import time

fpath = "/EPICS/autosave/gmodes.log"

### Function to write status to EPICS
def writestatus(msg):
    try:
        epics.caput("PINK:GMODES:status", msg)
        return True
    except:
        print("Err[write_status]: Failed posting status")
        return None

### Logging function
def writelog(msg):
    try:
        tnow = time.localtime()
        tstr = '['+\
            '{:04d}'.format(tnow.tm_year)+'.'+\
            '{:02d}'.format(tnow.tm_mon)+'.'+\
            '{:02d}'.format(tnow.tm_mday)+'|'+\
            '{:02d}'.format(tnow.tm_hour)+':'+\
            '{:02d}'.format(tnow.tm_min)+':'+\
            '{:02d}'.format(tnow.tm_sec)+'] '
        f= open(fpath,"a+")
        f.write(tstr+msg+'\n')
        f.close()
    except:
        print("[writelog]: Error writing log")
